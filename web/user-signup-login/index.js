'use strict'

const Routes = require('./routes')

function register (server, options) {
  server.dependency([ 'vision' ])

  server.route(Routes)
  server.log('info', 'Plugin registered: user signup and login')
}

exports.plugin = {
  register,
  name: 'user-signup-login',
  version: '1.0.0'
}
